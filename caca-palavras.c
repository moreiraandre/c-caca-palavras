#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "caca-palavras.h"

#define COLUNAS 26
#define LINHAS COLUNAS/2

char v[LINHAS][COLUNAS]; // VETOR DO CAÇA-PALAVRAS

int
        linha, // PERCORRE AS LINHAS
        coluna, // PERCORRE AS COLUNAS
        idxLetraEncontrar, // INDICA QUAL LETRA DEVE SER PROCURADA DA PALAVRA INFORMADA
/*
 * INFORMAÇÕES DA BUSCA ENCONTRADA
 */
        idxEncontrouLinhaIni = -1, // LINHA INICIAL
        idxEncontrouLinhaFim = -1, // LINHA FINAL
        idxEncontrouColunaIni = -1, // COLUNA INICIAL
        idxEncontrouColunaFim = -1; // COLUNA FINAL

/*
 * -1 = NÃO ENCONTROU
 *  1 = ESQUERDA DIREITA
 *  2 = DIREITA ESQUERDA
 *  3 = CIMA BAIXO
 *  4 = BAIXO CIMA
 */
int direcaoEncontrou = -1;

// USANDO NUMERAÇÃO DA TABELA ASCII PARA GERAR AS LETRAS
void gerarCacaPalavras() {
    int letra;

    for (linha = 0; linha < LINHAS; linha++) {
        for (coluna = 0; coluna < COLUNAS; coluna++) {
            do {
                letra = rand() % 123; // GERA UM NÚMERO INTEIRO ENTRE ZERO E 123
            } while (letra < 97); // GARANTE QUEO NÚMERO GERADO NÃO SEJA MENOR QUE 97. 97 A 123 ALFABETO MINÚSCULO.
            v[linha][coluna] = (char) letra; // CONVERTE O NÚMERO PARA LETRA E ATRIBUI AO VETOR
        }
    }
}

char getMaiuscula(char letra) {
    int numLetra = (int) letra; // IDENTIFICA O VALOR INTEIRO DA LETRA
    numLetra -= 32; // SUBTRAI 32 PARA ENCONTRAR A VALOR DA LETRA MAIUSCULA (TABELA ASCII)
    return (char) numLetra; // RETORNA O INTEIRO CONVERTIDO NA LETRA
}

void imprimir() {
    printf("STATUS BUSCA: ");
    switch (direcaoEncontrou) {
        case 1:
            printf("ENCONTRADA NA DIREÇÃO *ESQUERDA DIREITA*!\n");
            break;
        case 2:
            printf("ENCONTRADA NA DIREÇÃO *DIREITA ESQUERDA*!\n");
            break;
        case 3:
            printf("ENCONTRADA NA DIREÇÃO *CIMA BAIXO*!\n");
            break;
        case 4:
            printf("ENCONTRADA NA DIREÇÃO *BAIXO CIMA*!\n");
            break;
        default:
            printf("NÃO ENCONTRADA!\n");
    }
    printf("---------------------------------------------------------------------------------\n");

    printf("    "); // ESPAÇAMENTO ESQUERDO PARA ALINHAR O NÚMERO DAS COLUNAS COM AS LETRAS
    for (coluna = 0; coluna < COLUNAS; coluna++)
        printf("%02d ", coluna); // NÚMERO DAS COLUNAS
    printf("\n");

    // IMPRIMINDO SETAS COLUNAS ENCONTRADAS
    printf("   "); // ESPAÇAMENTO ESQUERDO PARA ALINHAR AS SETAS DAS COLUNAS ENCONTRADAS DAS COLUNAS COM AS LETRAS
    for (coluna = 0; coluna < COLUNAS; coluna++) {
        if (direcaoEncontrou == 1) {
            if ((coluna >= idxEncontrouColunaIni) && (coluna <= idxEncontrouColunaFim))
                printf("  v"); // IMPRIMI UMA SETA NAS COLUNAS ENCONTRADAS
            else
                printf("   ");
        } else if (direcaoEncontrou == 2) {
            if ((coluna <= idxEncontrouColunaIni) && (coluna >= idxEncontrouColunaFim))
                printf("  v"); // IMPRIMI UMA SETA NAS COLUNAS ENCONTRADAS
            else
                printf("   ");
        } else if (direcaoEncontrou == 3) {
            if (coluna == idxEncontrouColunaIni)
                printf("  v"); // IMPRIMI UMA SETA NAS COLUNAS ENCONTRADAS
            else
                printf("   ");
        } else if (direcaoEncontrou == 4) {
            if (coluna == idxEncontrouColunaIni)
                printf("  v"); // IMPRIMI UMA SETA NAS COLUNAS ENCONTRADAS
            else
                printf("   ");
        }
    }
    // *** FIM IMPRIMINDO SETAS COLUNAS ENCONTRADAS ***

    // CASO A BUSCA TENHA SIDO ENCONTRADA SERÃO SALTADAS 2 LINHAS POR CAUSA DA SETA DA COLUNA QUE INDICA A LETRA ENCONTRADA
    if (idxEncontrouColunaFim != -1)
        printf("\n\n");
    else
        printf("\n");

    for (linha = 0; linha < LINHAS; linha++) {
        // IMPRIMINDO SETAS E NÚMEROS DE LINHAS
        if (direcaoEncontrou < 3) {
            if (linha == idxEncontrouLinhaIni)
                printf("%02d> ", linha); // NÚMERO DAS LINHAS COM SETA DE ENCONTRADO
            else
                printf("%02d  ", linha); // NÚMERO DAS LINHAS
        } else {
            if (direcaoEncontrou == 3) {
                if ((linha >= idxEncontrouLinhaIni) && (linha <= idxEncontrouLinhaFim))
                    printf("%02d> ", linha); // NÚMERO DAS LINHAS COM SETA DE ENCONTRADO
                else
                    printf("%02d  ", linha); // NÚMERO DAS LINHAS
            } else if (direcaoEncontrou == 4) {
                if ((linha <= idxEncontrouLinhaIni) && (linha >= idxEncontrouLinhaFim))
                    printf("%02d> ", linha); // NÚMERO DAS LINHAS COM SETA DE ENCONTRADO
                else
                    printf("%02d  ", linha); // NÚMERO DAS LINHAS
            }
        }
        // *** FIM IMPRIMINDO SETAS E NÚMEROS DE LINHAS ***

        // IMPRIMINDO LETRAS MINUSCULAS E MAIUSCULAS (BUSCA ENCONTRADA) DE ACORDO COM O TIPO DE BUSCA ENCONTRADA
        for (coluna = 0; coluna < COLUNAS; coluna++) {
            if (direcaoEncontrou == 1) {
                if ((linha == idxEncontrouLinhaIni)
                    && ((coluna >= idxEncontrouColunaIni) && (coluna <= idxEncontrouColunaFim)))
                    printf("%2c ", getMaiuscula(v[linha][coluna])); // LETRAS
                else
                    printf("%2c ", v[linha][coluna]); // LETRAS
            } else if (direcaoEncontrou == 2) {
                if ((linha == idxEncontrouLinhaIni)
                    && ((coluna <= idxEncontrouColunaIni) && (coluna >= idxEncontrouColunaFim)))
                    printf("%2c ", getMaiuscula(v[linha][coluna])); // LETRAS
                else
                    printf("%2c ", v[linha][coluna]); // LETRAS
            } else if (direcaoEncontrou == 3) {
                if (((linha >= idxEncontrouLinhaIni) && (linha <= idxEncontrouLinhaFim))
                    && (coluna == idxEncontrouColunaIni))
                    printf("%2c ", getMaiuscula(v[linha][coluna])); // LETRAS
                else
                    printf("%2c ", v[linha][coluna]); // LETRAS
            } else if (direcaoEncontrou == 4) {
                if (((linha <= idxEncontrouLinhaIni) && (linha >= idxEncontrouLinhaFim))
                    && (coluna == idxEncontrouColunaIni))
                    printf("%2c ", getMaiuscula(v[linha][coluna])); // LETRAS
                else
                    printf("%2c ", v[linha][coluna]); // LETRAS
            } else
                printf("%2c ", v[linha][coluna]); // LETRAS
        }
        printf("\n");
    }
}

// REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO
void zerarBusca() {
    idxEncontrouLinhaIni = -1;
    idxEncontrouLinhaFim = -1;
    idxEncontrouColunaIni = -1;
    idxEncontrouColunaFim = -1;
    direcaoEncontrou = -1;
}

// UTILIZADA NAS DEMAIS FUNÇÕES PARA VERIFICAR SE A LETRA FOI ENCONTRADA
void comparacao(int direcao, char *palavra) {
    if (idxLetraEncontrar == 0) { // ENCONTROU A PRIMEIRA LETRA
        idxEncontrouLinhaIni = linha;
        idxEncontrouColunaIni = coluna;
        idxEncontrouLinhaFim = linha;
        idxEncontrouColunaFim = coluna;
    } else if (idxLetraEncontrar == (int) strlen(palavra) - 1) { // ENCONTROU A ÚLTIMA LETRA
        idxEncontrouLinhaFim = linha;
        idxEncontrouColunaFim = coluna;
        direcaoEncontrou = direcao;
    } else { // ENCONTROU PRÓXIMA LETRA
        idxEncontrouLinhaFim = linha;
        idxEncontrouColunaFim = coluna;
    }

    idxLetraEncontrar++;
}

int buscaEsqDir(char *palavra) {
    linha = -1;
    // PERCORRE AS 'LINHAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
    while ((++linha < LINHAS) && (idxLetraEncontrar < (int) strlen(palavra))) {

        coluna = -1;
        // PERCORRE AS 'COLUNAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
        while ((++coluna < COLUNAS) && (idxLetraEncontrar < (int) strlen(palavra))) {
            if (v[linha][coluna] ==
                palavra[idxLetraEncontrar]) { // A LETRA TUAL DO VETOR É IGUAL A LETRA ATUAL DA BUSCA
                comparacao(1, palavra);
            } else if (idxEncontrouColunaFim != -1) {
                // NÃO ENCONTROU PRÓXIMA LETRA
                zerarBusca(); // REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO
                idxLetraEncontrar = 0; // REINICIA A LETRA DA BUSCA
                coluna--; // VOLTA A COLUNA PORQUE AO INVÉS DE SER A PRÓXIMA LETRA DA BUSCA ELA PODE SER A PRIMEIRA
            }
        }
    }

    return direcaoEncontrou;
}

int buscaDirEsq(char *palavra) {
    linha = -1;
    // PERCORRE AS 'LINHAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
    while ((++linha < LINHAS) && (idxLetraEncontrar < (int) strlen(palavra))) {

        coluna = COLUNAS + 1;
        // PERCORRE AS 'COLUNAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
        while ((--coluna > -1) && (idxLetraEncontrar < (int) strlen(palavra))) {
            if (v[linha][coluna] ==
                palavra[idxLetraEncontrar]) { // A LETRA TUAL DO VETOR É IGUAL A LETRA ATUAL DA BUSCA
                comparacao(2, palavra);
            } else if (idxEncontrouColunaFim != -1) {
                printf("\n-->4\n");
                // NÃO ENCONTROU PRÓXIMA LETRA
                zerarBusca(); // REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO
                idxLetraEncontrar = 0; // REINICIA A LETRA DA BUSCA
                coluna++; // VOLTA A COLUNA PORQUE AO INVÉS DE SER A PRÓXIMA LETRA DA BUSCA ELA PODE SER A PRIMEIRA
            }
        }
    }

    return direcaoEncontrou;
}

int buscaCimaBaixo(char *palavra) {
    coluna = -1;
    // PERCORRE AS 'COLUNAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
    while ((++coluna < COLUNAS) && (idxLetraEncontrar < (int) strlen(palavra))) {
        linha = -1;
        // PERCORRE AS 'LINHAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
        while ((++linha < LINHAS) && (idxLetraEncontrar < (int) strlen(palavra))) {
            if (v[linha][coluna] ==
                palavra[idxLetraEncontrar]) { // A LETRA TUAL DO VETOR É IGUAL A LETRA ATUAL DA BUSCA
                comparacao(3, palavra);
            } else if (idxEncontrouColunaFim != -1) {
                // NÃO ENCONTROU PRÓXIMA LETRA
                zerarBusca(); // REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO
                idxLetraEncontrar = 0; // REINICIA A LETRA DA BUSCA
                linha--; // VOLTA A COLUNA PORQUE AO INVÉS DE SER A PRÓXIMA LETRA DA BUSCA ELA PODE SER A PRIMEIRA
            }
        }
    }

    return direcaoEncontrou;
}

int buscaBaixoCima(char *palavra) {
    coluna = -1;
    // PERCORRE AS 'COLUNAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
    while ((++coluna < COLUNAS) && (idxLetraEncontrar < (int) strlen(palavra))) {
        linha = LINHAS + 1;
        // PERCORRE AS 'LINHAS' ATÉ TERMINAR O VETOR OU ENCONTRAR A PALAVRA
        while ((--linha > -1) && (idxLetraEncontrar < (int) strlen(palavra))) {
            if (v[linha][coluna] ==
                palavra[idxLetraEncontrar]) { // A LETRA TUAL DO VETOR É IGUAL A LETRA ATUAL DA BUSCA
                comparacao(4, palavra);
            } else if (idxEncontrouColunaFim != -1) {
                // NÃO ENCONTROU PRÓXIMA LETRA
                zerarBusca(); // REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO
                idxLetraEncontrar = 0; // REINICIA A LETRA DA BUSCA
                linha++; // VOLTA A COLUNA PORQUE AO INVÉS DE SER A PRÓXIMA LETRA DA BUSCA ELA PODE SER A PRIMEIRA
            }
        }
    }

    return direcaoEncontrou;
}

/*
 * RESPONSÁVEL POR EXECUTAR O PRÓXIMO TIPO DE BUSCA CASO A ANTERIOR NÃO TENHA ENCONTRADO OU RESPONDER AO PROGRAMA
 * PRINCIPAL QUE A BUSCA NÃO FOI ENCONTRADA.
 */
int buscar(char *palavra) {
    idxLetraEncontrar = 0; // COMEÇA A PESQUISAR DA PRIMEIRA LETRA DA PALAVRA QUE SERÁ BUSCADA
    zerarBusca();  // REINICIA OS ÍNDICES DO INTERVALO ENCONTRADO

    if (buscaEsqDir(palavra) == -1)
        if (buscaDirEsq(palavra) == -1)
            if (buscaCimaBaixo(palavra) == -1)
                buscaBaixoCima(palavra);

    return direcaoEncontrou;
}
