## Trabalho Caça Palavras em C
**Professor**: Nivaldi Calonego Junior

## Problema
Procurar em uma matriz bidimensional de caracteres uma subcadeia.

A busca deve ser realizando considerando as opções abaixo até que seja encontrada
ou percorrido todas:
- Esquerda para direita
- Direira para esquerda
- Cima para baixo
- Baixo para cima

A implementação deve ser feita na linguagem **C**.

## Clonar projeto
```
git clone https://gitlab.com/moreiraandre/c-caca-palavras.git
```

## Executar projeto
Entre no diretório criado (**cd c-caca-palavras**) e execute no cmd o comando abaixo.
```
./exec.sh
```