#include <stdio.h>
#include <string.h> // stricmp()
#include <stdlib.h>
#include "caca-palavras.h"

int main() {
    char palavra[26]; // REEBE PELO TECLADO A PAVRA A SER BUSCADA OU 0 (ZERO) PARA FINALIZAR O PROGRAMA

    gerarCacaPalavras(); // PREENCHE O VETOR COM LETRAS ALEATÓRIAS
    do {
        system("clear");
        printf("=================================================================================\n");
        printf("|                                  CAÇA PALAVRAS                                |\n");
        printf("=================================================================================\n");
        imprimir();
        printf("\nINFORME A PALAVRA PARA BUSCA OU 0 (ZERO) PARA SAIR: ");
        scanf(" %[^\n]s", palavra);

        // CASO O USUÁRIO NÃO QUEIRA SAIR DO PROGRAMA A BUSCA É REALIZADA COMA PALAVRA INFORMADA
        if (strcmp(palavra, "0") != 0)
            buscar(palavra);
    } while (strcmp(palavra, "0") != 0); // CASO O USUÁRIO INFORME ZERO O PROGRAMA É FINALIZADO

    return 0;
}